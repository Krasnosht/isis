#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <asm/uaccess.h> 
#include <linux/string.h>
#include <linux/sched.h>

#define MAX_MESSAGE_LENGTH 32768
#define COUNT_MINOR_CODE 1
#define BASEMINOR 0
#define TASK_INFO_LEN 60

const char * module_name = "hello";
const char * class_name = "teach_devices";

static char message[MAX_MESSAGE_LENGTH];
static int count;

static dev_t dev;
static struct cdev c_dev;
static struct class * cl;

static int my_open(struct inode *inode, struct file *file);
static int my_close(struct inode *inode, struct file *file);
static ssize_t my_read(struct file *filp, char *buffer, size_t length,\
 loff_t * offset);
static ssize_t my_write(struct file *filp, const char *buff, size_t len,\
 loff_t * off);


static struct file_operations hello_fops =
{
	.owner = THIS_MODULE,
	.open = my_open,
	.release = my_close,
	.read = my_read,
	.write = my_write
};

static int __init hello_init(void) /* Initialization */
{
	int retval;
	bool allocated = false;
	bool created = false;
	cl = NULL;

	retval = alloc_chrdev_region(&dev, BASEMINOR, COUNT_MINOR_CODE,\
	 module_name);
	if (retval)
		goto err;

	allocated = true;
	printk(KERN_INFO "Major number = %d Minor number = %d\n",
	 	   MAJOR(dev), MINOR(dev));
	
	/*
	 * struct class * class_create (struct module * owner, const char * name);							
	 */

	cl = class_create(THIS_MODULE, class_name);
	if (!cl) {
		retval = -1;
		goto err;
	}


	if (device_create(cl, NULL, dev, NULL, module_name) == NULL) {
		retval = -1;
		goto err;
	}
	created = true;

	/*
	 * void cdev_init (	struct cdev * cdev,
 	 * const struct file_operations * fops);
	 */

	cdev_init(&c_dev, &hello_fops);


	retval = cdev_add(&c_dev, dev, COUNT_MINOR_CODE);
	if (retval)
		goto err;

	printk(KERN_INFO "Hello: registered");
	return 0;

err:
	printk("Hello: initialization failed with code %08x\n", retval);

	/* delete the device, free dev, delete the class */

	if (created)
		device_destroy(cl, dev);

	if (allocated)
		unregister_chrdev_region(dev, COUNT_MINOR_CODE);

	if (cl)
		class_destroy(cl);

	return retval;
}

static int my_open(struct inode *inode, struct file *file)
{
	/*
	 * The VFS inode data structure holds 
	 * information about a file or directory on disk.
	 */
    struct task_struct *task;

	for_each_process(task) {
		int i = 0;
		int task_len;
		char task_info[TASK_INFO_LEN];

		sprintf(task_info, "Task %s [pid = %d]\n", task->comm, task->pid);
        /* task_len - actual length taks_info */
		task_len = strlen(task_info);

		while (i < task_len && count < MAX_MESSAGE_LENGTH) {
			message[count] = task_info[i];
			count++;
			i++;
		}

    }

	if (count < MAX_MESSAGE_LENGTH) {
		message[count] = '\0';
		count = 0;
	}
	count = 0;
	return 0;
}

static int my_close(struct inode *inode, struct file *file)
{
	return 0;
}

static ssize_t my_read(struct file *filp,  
					   char *buffer, /* data buffer */
					   size_t length, /* buffer length */
					   loff_t * offset)
{
	


	if (count <= MAX_MESSAGE_LENGTH) {
		if (message[count] == 0) {
			printk("qwerty\n");
			count = 0;
			return 0;
		}
        /*copy_to_user(where to, where from, size)*/

		if (copy_to_user(buffer, &message[count], sizeof(char)))
			return -EFAULT;
		else {
			count++;
			return sizeof(char);
		}
	}
	count = 0;
	return 0;/* the number of bytes returned by the driver in the buffer */
}

static ssize_t my_write(struct file *filp, const char *buff,
						size_t len, loff_t * off)
{
	//#define EINVAL          22      /* Invalid argument */
    return -EINVAL;
}


static void __exit hello_exit(void) /* Deinitialization */
{
    printk(KERN_INFO "Hello: unregistered\n");
    device_destroy (cl, dev);
    unregister_chrdev_region (dev, COUNT_MINOR_CODE);
    class_destroy (cl);
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Daniil Krasnoshtanov");
MODULE_DESCRIPTION("Simple kernel module");
