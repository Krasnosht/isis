/* hello.c – simple loadable kernel module */
#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/sched.h>

/* Function for writing executable processes to the system log with the calculation of their number  */

static void print_process_info(void)
{
    struct task_struct *task;
    size_t process_counter = 0;
    
    for_each_process(task) {
        printk("Task %zu:",process_counter + 1);
        printk(" %s [%d] \n", task->comm, task->pid);
        process_counter++;
    }
    printk("Process number: %zu\n", process_counter);
}

static int __init hello_init(void) /* Initialization */
{
    /* Outputting a message to the system log */
    printk(KERN_INFO "Hello: registered\n");
    print_process_info();
	return 0;
}

static void __exit hello_exit(void) /* Deinitialization */
{
    /* Outputting a message to the system log */
	printk(KERN_INFO "Hello: unregistered\n");
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Daniil Krasnoshtanov");
MODULE_DESCRIPTION("Simple kernel module");	
